/**
 * Created by ronald8192 on 31/5/2016.
 */


var buControllers = angular.module('buControllers', []);

buControllers.controller('homeCtrl', ['$scope', '$http', '$routeParams',function ($scope, $http, $routeParams) {}]);
buControllers.controller('peopleCtrl', ['$scope', '$http', '$routeParams',function ($scope, $http, $routeParams) {}]);
buControllers.controller('aboutusCtrl', ['$scope', '$http', '$routeParams',function ($scope, $http, $routeParams) {}]);
buControllers.controller('contactCtrl', ['$scope', '$http', '$routeParams',function ($scope, $http, $routeParams) {}]);
buControllers.controller('newsCtrl', ['$scope', '$http', '$routeParams',function ($scope, $http, $routeParams) {}]);
buControllers.controller('researchCtrl', ['$scope', '$http', '$routeParams',function ($scope, $http, $routeParams) {}]);